#addin "nuget:https://api.nuget.org/v3/index.json?package=Cake.HockeyApp&version=0.6.0"
// #r "C:\Users\rlittles\Source\github\Cake.HockeyApp\src\Cake.HockeyApp\bin\Debug\net45\Cake.HockeyApp.dll"

Task("Upload-Android")
    .Does(() =>
    {
        var zip = File("./src/Droid/bin/" + configuration + "/com.hockeyapp.cake.apk");

        UploadToHockeyApp(zip, null, new HockeyAppUploadSettings
        {
			ApiToken = "43cb041b5b9b4d008f1b9a2956f716df",
            AppId = "905fcf71432b47ba8f0c2efa465677a2",
            Status = DownloadStatus.Allowed,
            Version = "1",
            //ApiBaseUrl = "https://upload.hockeyapp.net/"
        });
    });

Task("Upload-iOS")
    .WithCriteria(IsOSX)
    .Does(() =>
    {
        // var zip = File("./src/iOS/bin/" + configuration + "/com.hockeyapp.cake.ipa");

        // UploadToHockeyApp(zip, null, new HockeyAppUploadSettings
        // {
		// 	ApiToken = "43cb041b5b9b4d008f1b9a2956f716df",
        //     AppId = "905fcf71432b47ba8f0c2efa465677a2",
        //     Status = DownloadStatus.Allowed,
        //     Version = "1",
        //     //ApiBaseUrl = "https://upload.hockeyapp.net/"
        // });
    });