Task("Clean")
    .WithCriteria(BuildSystem.IsLocalBuild)
    .Does(() => 
	{
        CleanDirectory("./artifacts");
        CleanDirectory("./Cake.HockeyApp/Cake.HockeyApp/obj");
        CleanDirectory("./Cake.HockeyApp/Cake.HockeyApp/bin");
    });

Task("Restore")
    .Does(() => 
	{
        NuGetRestore("./Cake.HockeyApp.sln");
    });