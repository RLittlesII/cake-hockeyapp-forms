#addin "nuget:https://api.nuget.org/v3/index.json?package=Cake.Xamarin&version=1.3.0.15"

Task("Package-Android")
    .Does(() =>
    {
		var filePath = File("./src/Droid/Cake.HockeyApp.Droid.csproj");
        AndroidPackage(filePath, false, (settings) => settings.SetVerbosity(Verbosity.Minimal).SetConfiguration(configuration));
    });

Task("Package-iOS")
    .WithCriteria(platform == PlatformFamily.OSX)
    .Does(() =>
    {
        var solution = File("./Cake.HockeyApp.sln");
        MDToolArchive(solution, "Cake.HockeyApp.iOS", settings => 
        {
            settings.Configuration = "Release|iPhone";
        });

    });