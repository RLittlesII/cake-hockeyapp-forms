////////////////////////////////////
// ARGUMENTS
////////////////////////////////////
var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var platform = Context.Environment.Platform.Family;
var IsOSX = Context.Environment.Platform.Family == PlatformFamily.OSX;

////////////////////////////////////
// LOAD
////////////////////////////////////
#load "./build/common.cake"
#load "./build/hockey.cake"
#load "./build/xamarin.cake"

////////////////////////////////////
// DEPENDENCIES
////////////////////////////////////
Task("PreBuild")
    .IsDependentOn("Restore");

Task("Build")
    .IsDependentOn("Package-Android")
    .IsDependentOn("Package-iOS");

Task("PostBuild")
    .IsDependentOn("Upload-Android")
    .IsDependentOn("Upload-iOS");

Task("Default")
    .IsDependentOn("PreBuild")
    .IsDependentOn("Build")
    .IsDependentOn("PostBuild")
    .Finally(() =>
    {
    });

////////////////////////////////////
// EXECUTE
////////////////////////////////////
RunTarget(target);